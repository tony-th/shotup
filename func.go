package shotuploader

import (
	"net/http"
	"os"
)

func GetFileContentType(f *os.File) string {
	buffer := make([]byte, 512)
	_, err := f.Read(buffer)
	if err != nil {
		return "unknown"
	}
	contentType := http.DetectContentType(buffer)
	return contentType
}
