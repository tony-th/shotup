package shotuploader

import (
	"errors"
	"log"
	"net/http"
	"os"
	"time"
)

type HttpHandler struct {
	config ConfigMap
	client *http.Client
}

func (h *HttpHandler) Handle(file string) error {
	log.Println("http handler processes: ", file)
	pFile , err := os.Open(file)
	if err != nil {
		return errors.New("open file error: " + err.Error())
	}
	defer pFile.Close()

	method, ok := h.config["method"].(string)
	if !ok || method == "" {
		method = "POST"
	}
	url := h.config["url"].(string)

	request, err := http.NewRequest("POST", url, pFile)
	if err != nil {
		return err
	}
	request.Header.Set("User-Agent", "shotuploader/v0.0")
	response, err := h.client.Do(request)
	if err != nil {
		return err
	}
	log.Printf("http response: %d\n", response.StatusCode)
	_ = request.Body.Close()

	return nil
}

func NewHttpHandler(cfg ConfigMap, appCfg *AppConfig) (error, Handler) {
	handler := &HttpHandler{
		config: cfg,
	}

	handler.client = &http.Client{
		Timeout: time.Duration(15 * time.Second),
	}

	return nil, handler
}
