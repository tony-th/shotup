package main

import (
	"github.com/fsnotify/fsnotify"
	"github.com/getlantern/errors"
	"github.com/getlantern/systray"
	"gitlab.com/kingsley90/shotuploader"
	"gopkg.in/yaml.v2"
	"io/ioutil"
	"log"
	"os"
	"os/signal"
)

type noopCh chan struct{}

func main() {
	systray.Run(onReady, onExit)
}

func loopWatcher(watcher *fsnotify.Watcher, handle func(filename string), stop noopCh, done noopCh) {

	for {
		select {
		case event, ok := <-watcher.Events:
			if !ok {
				return
			}
			if event.Op&fsnotify.Create == fsnotify.Create {
				handle(event.Name)
			}
		case err, ok := <-watcher.Errors:
			if !ok {
				return
			}
			log.Println("watcher error: ", err)
		case <-stop:
			log.Println("stop watcher")
			close(done)
			return
		}
	}
}

func onReady() {
	done := make(noopCh)
	stop := make(noopCh)
	signalCh := make(chan os.Signal, 1)

	err, config := loadConfig()
	if err != nil {
		log.Fatal(err)
	}
	go manipulateTray(config, stop, done)

	err, app := shotuploader.NewApp(config)

	watcher, err := fsnotify.NewWatcher()
	if err != nil {
		log.Fatal("failed to init watcher: ", err)
	}
	defer watcher.Close()

	signal.Notify(signalCh, os.Interrupt)
	go func() {
		<-signalCh
		log.Println("stopping ...")
		close(stop)
	}()

	err = watcher.Add(config.WatchDir)
	if err != nil {
		log.Fatal("start watching failed: ", err)
	}

	log.Println("start watching: ", config.WatchDir)
	loopWatcher(watcher, app.Handle, stop, done)
}

func loadConfig() (error, *shotuploader.AppConfig) {
	bWhiteIconData, err := ioutil.ReadFile("resource/white.png")
	if err != nil {
		log.Println("failed to read icon file: ", err)
	}
	bBlueIconData, err := ioutil.ReadFile("resource/blue.png")
	if err != nil {
		log.Println("failed to read icon file: ", err)
	}
	bConfig, err := ioutil.ReadFile("./config.yml")
	if err != nil {
		return errors.New("failed to open config file: " + err.Error()), nil
	}
	config := &shotuploader.AppConfig{}
	err = yaml.Unmarshal(bConfig, config)
	if err != nil {
		return errors.New("failed to unmarshal config: " + err.Error()), nil
	}
	config.WhiteIconData = bWhiteIconData
	config.BlueIconData = bBlueIconData

	return nil, config
}

func manipulateTray(config *shotuploader.AppConfig, stop noopCh, done noopCh) {
	systray.SetIcon(config.WhiteIconData)
	systray.SetTooltip("upload new file to central storage automatically")
	mQuit := systray.AddMenuItem("Quit", "Quit")

	for {
		select {
		case <-mQuit.ClickedCh:
			close(stop)
		case <-done:
			systray.Quit()
		}
	}
}

func onExit() {

}
