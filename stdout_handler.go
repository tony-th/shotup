package shotuploader

import "fmt"

type StdoutHandler struct {
	config ConfigMap
}

func (h *StdoutHandler) Handle(file string) error {
	fmt.Printf("stdouthandler: %s \n", file)
	return nil
}

func NewStdoutHandler(cfg ConfigMap, config *AppConfig) (error, Handler) {
	handler := &StdoutHandler{cfg}

	return nil, handler
}
